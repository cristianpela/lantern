package home.crsk.lantern;

import android.app.KeyguardManager;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Camera;
import android.os.Binder;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;
import android.util.Pair;
import android.widget.RemoteViews;

import java.util.concurrent.TimeUnit;

public class LanternService extends Service {

    public static final String TAG = LanternService.class.getSimpleName();

    public static final String ACTION_FLASH = "home.crsk.service.action.START_FLASH";

    public static final String EXTRA_SPAMMED = "home.crsk.service.extra.SPAMMED";

    private static final long COOLDOWN_TIME = TimeUnit.SECONDS.toMillis(3);

    private static final long INTERVAL = TimeUnit.SECONDS.toMillis(1);

    private final IBinder mBinder = new LanternBinder();

    private BroadcastReceiver mReceiver;

    private Camera mCamera;

    private Vibrator mVibrator;

    private volatile boolean mOn;

    private volatile boolean cooldown;

    public LanternService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_STICKY;
    }

    @Override
    public void onCreate() {
        Log.i(TAG, "Service started and internal receiver registered");
        registerReceiver(mReceiver = new LanternServiceReceiver(),
                new IntentFilter(ACTION_FLASH));
        mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "Service destroyed and internal receiver unregistered");
        unregisterReceiver(mReceiver);
        destroyCamera();
    }

    public boolean isOn() {
        return mOn;
    }

    public void startFlash() {
        getCamera();
        handleFlash();
        mVibrator.vibrate(500);
        mOn = true;
        updateWidget();
        Log.i(TAG, "Flashlight started");
    }

    public void stopFlash() {
        handleFlash();
        mVibrator.vibrate(new long[]{0, 100, 1000}, -1);
        destroyCamera();
        mOn = false;
        updateWidget();
        Log.i(TAG, "Flashlight stopped");
    }

    private void handleFlash() {
        assert mCamera != null : "Camera is not initialized";
        String flashMode = (mOn) ? Camera.Parameters.FLASH_MODE_OFF : Camera.Parameters.FLASH_MODE_TORCH;
        Camera.Parameters params = mCamera.getParameters();
        params.setFlashMode(flashMode);
        mCamera.setParameters(params);
        mCamera.startPreview();
    }

    private void destroyCamera() {
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    private void getCamera() {
        if (mCamera == null) {
            try {
                mCamera = Camera.open();
            } catch (RuntimeException e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    private void handleAction() {
        if (!isOn()) {
            startFlash();
        } else
            stopFlash();
    }

    private void updateWidget() {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
        Pair<Integer, RemoteViews> widgetInfo = LanternAppWidget.getWidgetInfo(this);
        int widgetId = widgetInfo.first;
        RemoteViews remoteViews = widgetInfo.second;
        int iconId = (isOn()) ? R.drawable.ic_flash_on_white_24dp : R.drawable.ic_flash_off_white_24dp;
        remoteViews.setImageViewResource(R.id.widget_button, iconId);
        appWidgetManager.updateAppWidget(widgetId, remoteViews);

    }

    /**
     * This called in receiver. If the user pressed to many times on the phone vol button
     */
    private void handleActionSpam() {
        if (!cooldown) {
            new CountDownTimer(COOLDOWN_TIME, INTERVAL) {
                @Override
                public void onTick(long millisUntilFinished) {
                    if (millisUntilFinished == COOLDOWN_TIME) {// first pass then trigger the camera, else ignore
                        handleAction();
                    }
                    cooldown = true;
                }

                @Override
                public void onFinish() {
                    cooldown = false;
                }
            }.start();
        } else {
            Log.e(TAG, "Prevent any action during cooldown");
        }
    }

    public class LanternBinder extends Binder {
        LanternService getService() {
            return LanternService.this;
        }
    }

    public class LanternServiceReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra(EXTRA_SPAMMED, false)) {
                if (isScreenLocked(context)) {
                    handleActionSpam();
                }
            } else {
               handleAction();
            }
        }

        private boolean isScreenLocked(Context context) {
            KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
            return keyguardManager.inKeyguardRestrictedInputMode();
        }
    }


}
