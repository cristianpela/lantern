package home.crsk.lantern;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class LanternReceiver extends BroadcastReceiver {


    public static final String ACTION_VOLUME_CHANGED = "android.media.VOLUME_CHANGED_ACTION";

    public static final String ACTION_FLASH = "home.crsk.lantern.receiver.ACTION_FLASH";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(Intent.ACTION_BOOT_COMPLETED))
            context.startService(new Intent(context, LanternService.class));
        else if (action.equals(ACTION_VOLUME_CHANGED)) {
            context.sendBroadcast(new Intent(LanternService.ACTION_FLASH).putExtra(LanternService.EXTRA_SPAMMED, true));
        }else if(action.equals(ACTION_FLASH)) {//action call from widget
            //assure the service is started;
            context.startService(new Intent(context, LanternService.class));
            context.sendBroadcast(new Intent(LanternService.ACTION_FLASH));
        }
        Log.i(LanternReceiver.class.getSimpleName(), "Main Receiver " + intent.getAction());
    }
}
